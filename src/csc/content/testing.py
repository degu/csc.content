from plone.app.testing import PloneWithPackageLayer
from plone.app.testing import IntegrationTesting
from plone.app.testing import FunctionalTesting

import csc.content


CSC_CONTENT = PloneWithPackageLayer(
    zcml_package=csc.content,
    zcml_filename='testing.zcml',
    gs_profile_id='csc.content:testing',
    name="CSC_CONTENT")

CSC_CONTENT_INTEGRATION = IntegrationTesting(
    bases=(CSC_CONTENT, ),
    name="CSC_CONTENT_INTEGRATION")

CSC_CONTENT_FUNCTIONAL = FunctionalTesting(
    bases=(CSC_CONTENT, ),
    name="CSC_CONTENT_FUNCTIONAL")
