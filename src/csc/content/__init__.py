# -*- extra stuff goes here -*-

from zope.i18nmessageid import MessageFactory
CSCContentMessageFactory = MessageFactory('csc.content')

def initialize(context):
    """Initializer called when used as a Zope 2 product."""
