from zope.interface import Interface
from zope import schema

from csc.content import CSCContentMessageFactory as _

# Marker interfaces

class IHasDAMCode(Interface):
    """Marker interface for content with the DAM code field, which is enabled
    via a schema extender.
    
    This is applied to relevant types in content/configure.zcml.
    """

# Settings stored in the registry

class ICSCSettings(Interface):
    """Describes registry records
    """
    
    damCodes = schema.Tuple(
            title=_(u"Digital Asset Management codes"),
            description=_(u"Allowable values for the DAM code field"),
            value_type=schema.TextLine(),
        )

# Items of a registry

# class IXY(form.Schema):
#     """Describes register rows
#     """

#     x = schema.Int(
#         title=_(u"An X position"),
#         description=_(u"Each X"),
#         )

#     y = schema.Int(
#         title=_(u"An Y position"),
#         description=_(u"Each Y"),
#         )
