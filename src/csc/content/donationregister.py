# -*- coding: utf-8 -*-

from five import grok
from zope import schema
from plone.directives import form

from zope.interface import Invalid

from plone.namedfile.interfaces import IImageScaleTraversable
from plone.namedfile.field import NamedBlobImage

from zope.schema.vocabulary import SimpleVocabulary, SimpleTerm
from plone.app.textfield import RichText

from csc.content import CSCContentMessageFactory as _

class IDonationRegister(form.Schema):
    """A donation register.
    """

    month = schema.Choice(
        title=_(u"Mes"),
        description=_(u"Mes por el que se agrupan los registros"),
        vocabulary=SimpleVocabulary(
            [SimpleTerm(value=u'Enero', title=_(u'Enero')),
             SimpleTerm(value=u'Febrero', title=_(u'Febrero')),
             SimpleTerm(value=u'Marzo', title=_(u'Marzo')),
             SimpleTerm(value=u'Abril', title=_(u'Abril')),
             SimpleTerm(value=u'Mayo', title=_(u'Mayo')),
             SimpleTerm(value=u'Junio', title=_(u'Junio')),
             SimpleTerm(value=u'Julio', title=_(u'Julio')),
             SimpleTerm(value=u'Agosto', title=_(u'Agosto')),
             SimpleTerm(value=u'Septiebre', title=_(u'Septiembre')),
             SimpleTerm(value=u'Octubre', title=_(u'Octubre')),
             SimpleTerm(value=u'Noviembre', title=_(u'Noviembre')),
             SimpleTerm(value=u'Diciembre', title=_(u'Diciembre'))]
            ),
        required=True,
        )

    registers = RichText(
        title=_(u"Registros"),
        description=_(u"Los registros de donaciones deben ser ingresados en una tabla con las siguientes columnas: 1. Fecha (por ejemplo 'miercoles 1'), 2. Lugar (por ejemplo 'Liceo Técnico Femenino') y 3. Número de donantes (por ejemplo 14)."),
        )

class View(grok.View):
    """Default view (called "@@view"") for a contact.
    
    The associated template is found in donationregister_templates/view.pt.
    """
    
    grok.context(IDonationRegister)
    grok.require('zope2.View')
    grok.name('view')
